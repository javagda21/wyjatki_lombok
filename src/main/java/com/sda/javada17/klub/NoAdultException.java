package com.sda.javada17.klub;

public class NoAdultException extends RuntimeException {
    public NoAdultException() {
        super("Nie możesz wejść, jesteś niepełnoletni.");
    }
}
