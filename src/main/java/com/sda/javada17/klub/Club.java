package com.sda.javada17.klub;

public class Club {
    public void enter(Person person){
        if(person.getAge() < 18){
            throw new NoAdultException();
        }
        System.out.println("Witaj w klubie!");
    }
}
