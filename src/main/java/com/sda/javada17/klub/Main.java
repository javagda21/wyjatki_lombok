package com.sda.javada17.klub;

public class Main {
    public static void main(String[] args) {
        Club club = new Club();

        Person person = new Person("Marianek", "Mariański", 15);

        try {
            club.enter(person);
        }catch (NoAdultException nae){
            System.out.println(nae.getMessage());
        }

    }
}
