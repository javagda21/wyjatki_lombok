package com.sda.javada17;

import java.util.Scanner;

public class ZadanieDzielenie {
    public static void main(String[] args) {
        //todo 1: pobrać od użtkownika a i b;
        //todo 2: wywołać metodę podziel z parametrami
        // todo 3: obsłużyć wyjątek rzucany przez metodę
        // todo 4: jeśli wystąpił wyjątek poinformuj o błędzie. Jeśli nie - wypisz wynik.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj A:");
        int a = scanner.nextInt();
        System.out.println("Podaj B:");
        int b = scanner.nextInt();

        try {
            double wynik = podziel(a, b);

            System.out.println("Wynik: " + wynik);
        } catch (Exception e) {
            System.out.println("Błąd: " + e.getMessage());
        }
    }

    public static double podziel(int a, int b) throws Exception {
        if (b == 0) {
            throw new Exception("Buuu! Nie dziel przez 0!");
        }
        return a / b;
    }
}
