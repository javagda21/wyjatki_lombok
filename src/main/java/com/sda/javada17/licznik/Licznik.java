package com.sda.javada17.licznik;

public class Licznik {
    private static final int OGRANICZENIE = 5;
    private int licznik;

    public Licznik() {
    }

    public void dodaj() {
        if (licznik >= OGRANICZENIE) {
            throw new RuntimeException("Przekroczono maksymalną wartość!");
        }
        licznik++;
    }

    public void dodaj(int ile) throws Exception {
        if (licznik >= (OGRANICZENIE - ile)) {
            throw new Exception("Przekroczono maksymalną wartość!");
        }
        licznik += ile;
    }

}
