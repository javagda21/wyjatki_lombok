package com.sda.javada17;

import lombok.*;

@Data
//@Getter
//@Setter
//@ToString
//@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Osoba {

    private String name;
    private String surname;
    private int age;
}
