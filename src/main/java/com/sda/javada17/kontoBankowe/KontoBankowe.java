package com.sda.javada17.kontoBankowe;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KontoBankowe {
    private String nrKonta;
    private int stanKonta;

    public void zasilKonto(int oIle){
        stanKonta+=oIle;
    }

    public int wyplacZKonta(int ile){
        // todo: wyjątek i obsługę przypadku kiedy użytkownik wpisuje ujemną kwotę
        if(ile < 0){
            throw new UjemnaKwotaException();
        }
        if(stanKonta < ile){
            throw new BrakSrodkowException();
        }
        stanKonta-=ile;
        return ile;
    }
}
