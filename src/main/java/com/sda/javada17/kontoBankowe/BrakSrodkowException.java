package com.sda.javada17.kontoBankowe;

public class BrakSrodkowException extends RuntimeException {
    public BrakSrodkowException() {
        super("Brak srodkow na koncie.");
    }
}
