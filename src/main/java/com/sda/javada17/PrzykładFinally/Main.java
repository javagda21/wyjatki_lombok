package com.sda.javada17.PrzykładFinally;

public class Main {
    public static void main(String[] args) {
        System.out.println(metoda());
    }

    public static double metoda() {
        int a = 3, b = 5;
        try {
            System.out.println("Cos");
            return b / a;
        } catch (Exception e) {
            System.out.println("Błąd");
        } finally {
            System.out.println("Finally");
        }

        System.out.println("Tylko taki komunikat");
        return 0;
    }
}
