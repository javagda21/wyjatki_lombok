package com.sda.javada17;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int liczba = 0;

        int liczbaDoPodzielenia = 5;

//        try {
//            int wynik = liczbaDoPodzielenia / liczba;
//            System.out.println(liczbaDoPodzielenia / liczba); // nie musimy obsługiwać // RuntimeException
//        }catch (ArithmeticException ae){
//            System.out.println("Wystąpił błąd");
//        }

//        System.out.println("Etam exception");
//        try {
//            // jawne - trzeba obsłużyć kiedy tylko jest PRAWDOPODOBIEŃSTWO ich wystąpienia // Excepion
//            InputStream stream = new FileInputStream(new File("pom.xml"));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        Main main = new Main();
//        main.metoda();
        metoda();
        System.out.println("Jupi!");
    }

    public static void metoda() {
        System.out.println("Jestem w metodzie");
        try {
            innametoda();
        } catch (RuntimeException re) {
            System.out.println("Przechwycony metoda!");
        }
        System.out.println("Opuszczam metodę");
    }

    private static void innametoda() {
        System.out.println("Wchodze glebiej");
        try {
            costamJeszczeGlebiej();
        } catch (Exception re) {
            System.out.println(re.getMessage());
            System.out.println("Przechwycony innametoda!");
        }
        System.out.println("opuszczam to glebiej");
    }

    private static void costamJeszczeGlebiej() throws Exception{
        System.out.println("Gleboko jestem.");
        throw new Exception("Cos sie stalo bubu!");
//        System.out.println("cokolwiek");
    }

}
